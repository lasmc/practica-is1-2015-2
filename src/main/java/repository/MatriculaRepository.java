package repository;

import domain.Matricula;

public interface MatriculaRepository extends BaseRepository<Matricula, Long>{


	String FindAlumnoByIdMatricula( Long idm );
	
	String FindCursoByIdMatricula( Long idm );
}