package repository.jpa;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import domain.Matricula;
import repository.MatriculaRepository;

@Repository
public class JpaMatriculaRepository extends JpaBaseRepository< Matricula , Long > 
implements MatriculaRepository{

	@Override
	public String FindAlumnoByIdMatricula( Long idm ){
		String querys = "SELECT a FROM Solicitud a WHERE a.id = :idm";
		TypedQuery< Matricula > consulta = entityManager.createQuery(querys, Matricula.class ); 
		return consulta.getParameter( "Alumnos", String.class ).toString();
		
	}
	
	@Override
	public String FindCursoByIdMatricula( Long idm ){
		String querys = "SELECT a FROM Solicitud a WHERE a.id = :idm";
		TypedQuery< Matricula > consulta = entityManager.createQuery(querys, Matricula.class );
		return consulta.getParameter("Cursos", String.class ).toString();
	}
	
}

	
